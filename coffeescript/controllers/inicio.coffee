app.controller 'InicioCrtl',['$scope','$http','$location', 'ngDialog', '$timeout', (scope, http,location, ngDialog,timeout) ->
	setupcol = 0
	scope.setupcol = (padre) ->
		h = $("."+padre+" > .altohijo").map( () ->
				return $(this).height()
			).get()
		maxHeight = Math.max.apply(null, h)
		$("."+padre+" > .altohijo").height(maxHeight)
		## console.log "altocol" + maxHeight
		return
	scope.setup = ()->
		scope.setupcol("altopadre1")
		scope.setupcol("altopadre2")
		scope.setupcol("altopadre3")
		return
	timeout scope.setup, 500
	## para el tema de responsive
	window.addEventListener("resize", scope.setup)
	return
]
